<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Scores extends Model{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $table = 'scores';
    use SoftDeletes;
    protected $fillable = [
        'english_name',
        'serbian_name',
        'user_entered_name',
        'deleted_at',
    ];
}
