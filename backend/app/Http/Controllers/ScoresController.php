<?php

namespace App\Http\Controllers;

use App\Models\Scores;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * The score controller
 */
class ScoresController extends Controller
{
    /**
     * @desc: Constructor
     */
    public function __construct()
    {
        // Constractor
    }
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function index(Request $request){
        $scores  = Scores::paginate(10);
        $response = array("status"=>0, "message"=>"No record found.");
        if(!empty($scores)){
            $response["status"] = 1;
            $response["data"] = $scores;
            $response["message"] = "";
        }
        return response()->json($response, 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function store(Request $request)
    {
        $post = $request->all();
        $rsp = array();
        $validator = Validator::make($post,[
            "english_name" => "required",
            "serbian_name" => "required",
            "user_entered_name" => "required",
        ]);
        if ($validator->fails()) {
            $rsp['message'] = $validator->errors();
            $rsp['status'] = 0;
            return response()->json($rsp, 400);
        }

        $insert = Scores::create($request->all());
        $rsp["status"] = $insert ? 1 : 0;
        $rsp["message"] = $insert ? "Inserted successfully.":"Failed to insert";

        return response()->json($rsp, 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    function show($id){
        $scores  = Scores::where("id",$id)->get();
        $response = array("status"=>0, "message"=>"No record found.");
        if(!empty($scores)){
            $response["status"] = 1;
            $response["data"] = $scores;
            $response["message"] = "";
        }
        return response()->json($response, 200);
    }
}
